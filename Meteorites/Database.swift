//
//  Database.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit
import CoreData

class Database: NSObject {
    
    static var appDelegate: AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func synchronize(delegate: SynchronizationIndicator?){
        DispatchQueue.main.async {
            delegate?.startIndicateSyncrhonization()
        }
        DispatchQueue(label: "Com").async {
            NASA.loadMeteorites(completation: { (meteorites) in
                DispatchQueue.main.async {
                    let del = self.appDelegate
                    let mainMoc = del.persistentContainer.viewContext
                    let moc = del.persistentContainer.newBackgroundContext()
                    moc.perform {
                        var errorOccured = false
                        for meteorite in meteorites{
                            if let id = meteorite["id"] as? String,
                                meteoritesWith(id: id, moc: moc).count == 0{
                                self.createMeteorite(data: meteorite, moc: moc)
                                do{
                                    try moc.save()
                                    try mainMoc.save()
                                }catch{
                                    print("fuck")
                                    errorOccured = true
                                }
                            }
                        }
                        if errorOccured {
                            //TODO: Handle sync error
                        }
                        DispatchQueue.main.async {
                            del.saveContext()
                            delegate?.stopIndicateSyncrhonization()
                        }
                    }
                }
            })
        }
    }
    
    class func createMeteorite(data: [String: AnyObject], moc: NSManagedObjectContext){
        let met = Meteorite(context: moc)
        met.id = data["id"] as? String
        met.name = data["name"] as? String
        if let geo = data["geolocation"] as? [String: AnyObject],
            let coors = geo["coordinates"] as? [Double],
            coors.count == 2{
            met.longtitude = coors[0]
            met.latitude = coors[1]
        }
        if let jsonDate = data["year"] as? String{
            met.time = Date.dateFromNASA(json: jsonDate)
        }
        if let massString = data["mass"] as? String,
            let mass = Int64(massString){
            met.mass = mass
        }
    }
    
    class func meteoritesWith(id: String, moc: NSManagedObjectContext) -> [Meteorite]{
        let request = NSFetchRequest<Meteorite>(entityName: "Meteorite")
        request.predicate = NSPredicate(format: "id == \(id)")
        return (try? moc.fetch(request)) ?? []
    }

}
