//
//  Location.swift
//  Meteorites
//
//  Created by Adam Salih on 30/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit
import CoreLocation

class Location: NSObject {
    
    static var shared: Location = Location()
    
    private var locationHandler: ((CLLocation?) -> Void)?
    
    let manager: CLLocationManager = CLLocationManager()
    
    var lastLocation: CLLocation?
    
    var enabled: Bool{
        let status = CLLocationManager.authorizationStatus()
        return status == .authorizedWhenInUse || status == .authorizedAlways
    }
    
    override init() {
        super.init()
        self.manager.delegate = self
    }
    
    func requestLocation(completation: ((CLLocation?) -> Void)?){
        self.locationHandler = completation
        self.manager.requestLocation()
    }
    
    // Returns a distance in km from user to meteorite.
    func distance(from meteorite: Meteorite) -> Double?{
        if enabled {
            if let distance = self.lastLocation?.distance(from: meteorite.location){
                return distance / 1000
            }
        }
        return nil
    }

}

extension Location: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationHandler?(nil)
        self.locationHandler = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.lastLocation = locations.first
        self.locationHandler?(locations.first)
        self.locationHandler = nil
    }
}
