//
//  GestureDelegate.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class GestureDelegate: NSObject {
    
    private var startingPoint: CGPoint = CGPoint(x: 0, y: 0)
    weak var main: MainViewController!
    
    init(main: MainViewController) {
        self.main = main
        super.init()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(gesture:)))
        main.listView.addGestureRecognizer(pan)
        main.listTitleView.addGestureRecognizer(tap)
    }
    
    @objc func tap(){
        main.listViewIsShown ? main.hideListView() : main.showListView()
    }
    
    @objc func pan(gesture: UIPanGestureRecognizer){
        switch(gesture.state){
        case .began:
            self.startingPoint = gesture.location(in: main.view)
        case .ended:
            let velocity = gesture.velocity(in: main.view)
            switch (velocity.y){
            case let v where v >= Constants.criticalVelocity: main.hideListView()
            case let v where v <= -Constants.criticalVelocity: main.showListView()
            default: main.listViewIsShown ? main.showListView() : main.hideListView()
            }
        default:
            let point = gesture.location(in: main.view)
            let delta = startingPoint.y - point.y
            let offset = main.listViewIsShown ? main.minOffset - delta : main.maxOffset - delta
            if main.minOffset <= offset || offset <= main.maxOffset{
                main.listViewOffset.constant = offset
            }
        }
    }

}
