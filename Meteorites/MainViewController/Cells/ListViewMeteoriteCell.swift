//
//  ListViewMeteoriteCell.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class ListViewMeteoriteCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func update(meteorite: Meteorite){
        self.nameLabel.text = meteorite.name
        self.massLabel.text = meteorite.mass.apropriateUnit
        let distance = Location.shared.distance(from: meteorite)?.prettyString ?? "-"
        self.distanceLabel.text = "\(distance) km"
    }

}
