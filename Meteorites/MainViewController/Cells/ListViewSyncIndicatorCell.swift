//
//  ListViewSyncIndicatorCell.swift
//  Meteorites
//
//  Created by Adam Salih on 30/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class ListViewSyncIndicatorCell: UITableViewCell {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.indicator.startAnimating()
    }

}
