//
//  ViewController.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

import UIKit
import MapKit

class MainViewController: UIViewController{
    //MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationButtonContainer: UIView!
    @IBOutlet weak var locationActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var listViewOffset: NSLayoutConstraint!
    @IBOutlet weak var listViewHeight: NSLayoutConstraint!
    @IBOutlet weak var listTitleView: UIView!
    @IBOutlet weak var dragIndicator: UIView!
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var detailView: MeteoriteDetailView!
    @IBOutlet weak var detailCompactConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailRegularConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailBotomSafeAreaOffset: NSLayoutConstraint!
    
    //MARK: variables
    var listViewIsShown: Bool = false
    var gestureDelegate: GestureDelegate!
    var mainModel: MainModel!
    
    //MARK: Constants
    let minOffset: CGFloat = Constants.minListViewOffset
    var maxOffset: CGFloat{
        return self.view.frame.height - self.listTitleView.frame.height
            - self.view.safeAreaInsets.top - self.view.safeAreaInsets.bottom
    }
    
    var viewRadius: CGFloat{
        var isX = UIDevice().userInterfaceIdiom == .phone
        isX = isX && UIScreen.main.nativeBounds.height == 2436
        return isX ? Constants.listViewRadiusX : Constants.listViewRadius
    }
    
    //MARK: - Methods
    override func viewDidLoad(){
        self.gestureDelegate = GestureDelegate(main: self)
        self.mainModel = MainModel(map: mapView, tableView: listTableView, main: self)
        self.mainModel.reload()
        self.listView.layer.cornerRadius = viewRadius
        self.detailView.layer.cornerRadius = viewRadius
        self.listViewOffset.constant = self.view.frame.height
        self.dragIndicator.layer.cornerRadius = self.dragIndicator.frame.height / 2
        self.locationButtonContainer.layer.cornerRadius = 8
        self.locationActivityIndicator.alpha = 0
        self.hideDetailView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.hideListView()
        Database.synchronize(delegate: self.mainModel)
    }
    
    override func viewDidLayoutSubviews() {
        let sizeClass = self.traitCollection.horizontalSizeClass
        if sizeClass == .compact{
            let safeArea = self.additionalSafeAreaInsets.bottom + self.additionalSafeAreaInsets.top
            let shrinkage = safeArea + minOffset
            self.listViewHeight.constant = -shrinkage
            self.detailBotomSafeAreaOffset.constant = 24 + self.view.safeAreaInsets.bottom + viewRadius
        }
    }
    
    @IBAction func showLocation(_ sender: Any) {
        Location.shared.manager.requestWhenInUseAuthorization()
        if Location.shared.enabled {
            self.locationActivityIndicator.startAnimating()
            UIView.animate(withDuration: 0.3) {
                self.locationActivityIndicator.alpha = 1
                self.locationButton.alpha = 0
            }
            Location.shared.requestLocation { (userLocation) in
                self.locationActivityIndicator.stopAnimating()
                UIView.animate(withDuration: 0.3) {
                    self.locationActivityIndicator.alpha = 0
                    self.locationButton.alpha = 1
                }
                if let location = userLocation{
                    self.hideListView()
                    self.listTableView.reloadData()
                    self.mapView.setCenter(location.coordinate, animated: true)
                }
            }
        }
    }
    
    func hideListView(){
        self.makeListView(shown: false)
    }
    
    func showListView(){
        self.makeListView(shown: true)
    }
    
    private func makeListView(shown: Bool, animated: Bool = true){
        self.listViewIsShown = shown
        self.listViewOffset.constant = shown ? self.minOffset : self.maxOffset
        if animated {
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func showDetailView(){
        self.makeDetailView(shown: true)
    }
    
    func hideDetailView(){
        self.makeDetailView(shown: false)
    }
    
    private func makeDetailView(shown: Bool){
        self.detailRegularConstraint.constant = shown ? 16 : -(self.detailView.frame.width + 50)
        self.detailCompactConstraint.constant = shown ? -viewRadius : -(self.detailView.frame.height + self.view.safeAreaInsets.bottom + 100)
    }
    
    func show(meteorite: Meteorite){
        self.mapView.showAnnotations([meteorite], animated: true)
        self.listViewOffset.constant = self.view.frame.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (bl) in
            self.detailView.change(meteorite: meteorite)
            self.showDetailView()
            UIView.animate(withDuration: 0.3) {
                self.listView.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func hideDetail(){
        self.hideDetailView()
        UIView.animate(withDuration: 0.3, animations: {
            self.listView.alpha = 1
            self.view.layoutIfNeeded()
        }) { (bl) in
            self.hideListView()
        }
        
    }
}
