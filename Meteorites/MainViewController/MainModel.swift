//
//  MainModel.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit
import MapKit
import CoreData

protocol SynchronizationIndicator{
    func startIndicateSyncrhonization()
    func stopIndicateSyncrhonization()
}

class MainModel: NSObject {
    
    weak var listTableView: UITableView?
    weak var mapView: MKMapView?
    weak var main: MainViewController?
    var controller: NSFetchedResultsController<Meteorite>?
    
    var isSyncing: Bool = false
    
    var meteorites: [Meteorite]{
        return self.controller?.fetchedObjects ?? []
    }
    
    var loadingCellIndexPath: IndexPath{
        return IndexPath(row: self.meteorites.count, section: 0)
    }
    
    init(map: MKMapView, tableView: UITableView, main: MainViewController) {
        self.mapView = map
        self.listTableView = tableView
        self.main = main
        super.init()
        self.listTableView?.delegate = self
        self.listTableView?.dataSource = self
    }
    
    func createController() -> NSFetchedResultsController<Meteorite>{
        let moc = Database.appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<Meteorite>(entityName: "Meteorite")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: nil)]
        let controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc,
                                                    sectionNameKeyPath: nil, cacheName: "Meteorites")
        controller.delegate = self
        try? controller.performFetch()
        return controller
    }
    
    func reload(){
        self.controller = self.createController()
        self.listTableView?.reloadData()
        self.mapView?.addAnnotations(self.meteorites)
    }
}

extension MainModel: SynchronizationIndicator{
    func startIndicateSyncrhonization() {
        self.isSyncing = true
        self.listTableView?.insertRows(at: [loadingCellIndexPath], with: .automatic)
    }
    
    func stopIndicateSyncrhonization() {
        self.isSyncing = false
        //TODO: resolve an issue that prevents NSFetchedResultsControllerDelegate to respond to changes in psc, when updating from background context
        self.reload() // meanwhile use force reload
//        self.listTableView?.deleteRows(at: [self.loadingCellIndexPath], with: .automatic)
    }
}

extension MainModel: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if index < self.meteorites.count{
            let meteorite = self.meteorites[index]
            self.main?.show(meteorite: meteorite)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meteorites.count + (isSyncing ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.meteorites.count{
            return tableView.dequeueReusableCell(withIdentifier: "LoadingCell")!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeteoriteCell") as! ListViewMeteoriteCell
        let meteorite = self.meteorites[indexPath.row]
        cell.update(meteorite: meteorite)
        self.mapView?.addAnnotation(meteorite)
        return cell
    }
}

extension MainModel: NSFetchedResultsControllerDelegate{
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
        self.listTableView?.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
        self.listTableView?.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?){
        switch type {
        case .update:
            let cell = self.listTableView?.cellForRow(at: indexPath!) as? ListViewMeteoriteCell
            cell?.update(meteorite: self.meteorites[indexPath!.row])
        case .insert:
            self.listTableView?.insertRows(at: [newIndexPath!], with: .automatic)
            let meteorite = self.meteorites[newIndexPath!.row]
            self.mapView?.addAnnotation(meteorite)
        case .delete:
            self.listTableView?.deleteRows(at: [indexPath!], with: .automatic)
            let meteorite = self.meteorites[indexPath!.row]
            self.mapView?.removeAnnotation(meteorite)
        case .move:
            self.listTableView?.reloadData()
        }
    }
}
