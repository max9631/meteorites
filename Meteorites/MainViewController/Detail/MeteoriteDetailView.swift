//
//  MeteoriteDetailView.swift
//  Meteorites
//
//  Created by Adam Salih on 30/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class MeteoriteDetailView: UIView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var distLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func change(meteorite: Meteorite){
        self.nameLabel.text = meteorite.name
        self.massLabel.text = meteorite.mass.apropriateUnit
        let distance = Location.shared.distance(from: meteorite)?.prettyString ?? "-"
        self.distLabel.text = "\(distance) km"
        self.dateLabel.text = meteorite.time?.fullDateString
    }

}
