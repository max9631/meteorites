//
//  NASA.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class NASA: NSObject {
    static let token: String = "THhtPVVG3TovCfxbxXoYTYvAU"
    
    class func loadMeteorites(completation:@escaping ((_ meteorites: [[String: AnyObject]]) -> Void)){
        var request = URLRequest(url: URL(string: "https://data.nasa.gov/resource/y77d-th95.json")!)
        request.httpMethod = "GET"
        request.addValue(self.token, forHTTPHeaderField: "X-App-Token")
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            if let data = data,
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments),
            let meteorites = json as? [[String:AnyObject]]{
                completation(meteorites)
            }
        }
        task.resume()
    }

}
