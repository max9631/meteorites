//
//  Int64.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

extension Int64 {
    fileprivate var kilo: Int64 { return 1000 }
    
    fileprivate var ton: Int64 { return 1000000 }
    
    var apropriateUnit: String{
        switch  self {
        case let m where m >= ton : return "Mass: \(self/ton)t"
        case let m where m >= kilo: return "Mass: \(self/kilo)kg"
        default: return "Mass: \(self)g"
        }
    }
}
