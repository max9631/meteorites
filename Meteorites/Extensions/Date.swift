//
//  Date.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

extension Date {
    static func dateFromNASA(json: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        return formatter.date(from: json)
    }
    
    var fullDateString: String{
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.dateFormat = "dd MMMM, h:mm"
        return formatter.string(from: self)
    }
}
