//
//  Meteorite.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit
import MapKit

extension Meteorite{
    var location: CLLocation{
        return CLLocation(latitude: CLLocationDegrees(self.latitude),
                          longitude: CLLocationDegrees(self.longtitude))
    }
}

extension Meteorite: MKAnnotation {
    
    public var title: String? {
        return self.name
    }
    
    public var subtitle: String? {
        return self.mass.apropriateUnit
    }
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: CLLocationDegrees(self.latitude),
                                      longitude: CLLocationDegrees(self.longtitude))
    }
}
