//
//  Double.swift
//  Meteorites
//
//  Created by Adam Salih on 30/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

extension Double {
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    var pretty: Double{
        return self.rounded(toPlaces: 1)
    }
    
    var prettyString: String{
        return "\(self.pretty)"
    }

}
