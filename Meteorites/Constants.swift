//
//  Constants.swift
//  Meteorites
//
//  Created by Adam Salih on 28/06/2018.
//  Copyright © 2018 Adam Salih. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let minListViewOffset: CGFloat = 32
    
    static let criticalVelocity: CGFloat = 700
    
    static var listViewRadius: CGFloat = 24
    
    static var listViewRadiusX: CGFloat = 44

}
